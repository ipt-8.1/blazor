﻿using System;

namespace ThermoSjck.Model
{
    public class DbEntry
    {
        public DateTime Timestamp { get; set; }
        public float RoomTemperature { get; set; }
        public float FlowTemperature { get; set; }
        public float WaterTankTemperature { get; set; }
        public bool PumpState { get; set; }
        public int ValveState { get; set; }

        public DbEntry(float roomTemperature, float flowTemperature, float waterTankTemperature, bool pumpState, int valveState)
        {
            Timestamp = DateTime.Now;
            RoomTemperature = roomTemperature;
            FlowTemperature = flowTemperature;
            WaterTankTemperature = waterTankTemperature;
            PumpState = pumpState;
            ValveState = valveState;
        }
    }
}
